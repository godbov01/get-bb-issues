A simple py script to pull all the issues and their comments from a Bitbucket repo using the Bitbucket API. The repo and the issue must be public for this script to work.

The script creates a file in the same dir as the py script for each issue.